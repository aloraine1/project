# Daisyworld Project README
This repository contains the code project for a Java implementation of a Daisyworld simulation model.
This code makes up the implementation section of my project portfolio, as well as the parts of the Outcome section that requires commented code and technical and user documentation to be supplied with the project.

Written by Adam Loraine, 3rd Year BSc Computer Science, University of East Anglia, Norwich
# Motivation
This project is done as part of the Coursework for module CMP-6013Y (Third Year Project) for Supervisor Dr Taoyang Wu
and project coordinator Dr Pierre Chardairre with Support from co-supervisor Dr Cock Van Oosterhout.
# Build Status
The construction of the simulation is completed.
The data produced by the simulation is useable for certain experiments, as shown in the final report, but does not directly compare to other models.
The data representation element of the project has not been completed.
# Technical Details
The code was built with Java 8 including JUnit4 for unit testing.
# Screenshots
![alt text](Screenshots/InitialConsoleScreenshot.png) 
#Installation, Setup and Use
The program should run when set up in a compatible environment. It is a console only application, as shown in the screenshots above.
Running the Main class should kick off the simulation starting instructions.
Accepting the default options in the first instance will set the run up with the following options:
* The size of the Map will be set to 50x50 tiles
* The simulation will run for 20,000 time-steps
* The map will be initialised with 10 initial daisies
* The default temperature accross the map will be started at 24 degrees celsius.
* The radiation intensity will be at a constant of 1 for the whole run-time.

In order to alter the radiation intensity function, lines 75 and 76 can be uncommented to implement an oscillating or linear function respectively.
These lines are as follows:
```java
// daisyMap.setRadiationIntensity(1 + 0.5 * (psdsd * Math.sin((3.14 * p1) / qwe)));
// daisyMap.setRadiationIntensity( daisyMap.getRadiationIntensity() + (1.0/10000));
```

If you do not choose to use the default options then the user will be prompted to enter each one in turn.

Note: In order for this program to run, a folder called 'output' must be created in the repository before the code will run successfuly.
This code saves its output into this folder. Also note that if results are generated, and then the program is run again, the results in the file
will be overwritten, so be sure to move/rename previous results prior to rerunning the simulation. 

#Tests
Unit tests for most major functions have been written with Junit4.
These tests are located in src/Test/Test and can be run as independent tests or as a whole test class in the traditional fashion.
#Credits
Some of the formulae and the general outline of this code has taken light inspiration from a daisyworld implementation by Ben Tatman ([GitHub](http://github.com/ThatPerson/Gaia)). This was a C implementation of a daisyworld model featuring DAE. This project extends this to include the concept of polyploidy.
