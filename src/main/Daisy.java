package main;


import java.util.Arrays;
import java.util.Map;
import java.util.Random;

/**
 * Class to represent the daisy object in the daisyworld model
 */
public class Daisy {


    //<editor-fold desc="Class Variables">
    // how far away offspring can be placed on the map
    private int dispersal;
    // how many offspring from each reproduction
    private int progeny;
    // how many time-steps the daisy has been alive
    private int age;
    // how many generations into the population is the individual
    private int generation;
    // is the daisy living
    private boolean alive;
    // the rate at which the daisy mutates (chance)
    private double mutation_rate;
    // represents the colour alleles
    private Colour colour;
    // temperature alleles array
    private double[] temp_alleles;
    // temperature of the position the daisy is inhabiting
    private double local_temp;
    // the arbitrary measure of how close to reproducing this instance is
    private double resources;
    // the ploidy of the daisy
    private int ploidy;
    //</editor-fold>

    //<editor-fold desc="Accessors and Mutators">

    /**
     *
     * @return
     */
    public int getPloidy() {
        return ploidy;
    }

    /**
     *
     * @param ploidy
     */
    public void setPloidy(int ploidy) {
        this.ploidy = ploidy;
    }

    /**
     *
     * @return
     */
    public int getDispersal() {
        return dispersal;
    }

    /**
     *
     * @param dispersal
     */
    public void setDispersal(int dispersal) {
        this.dispersal = dispersal;
    }

    /**
     *
     * @return
     */
    public int getProgeny() {
        return progeny;
    }

    /**
     *
     * @param progeny
     */
    public void setProgeny(int progeny) {
        this.progeny = progeny;
    }

    /**
     *
     * @return
     */
    public int getAge() {
        return age;
    }

    /**
     *
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     *
     * @return
     */
    public int getGeneration() {
        return generation;
    }

    /**
     *
     * @param generation
     */
    public void setGeneration(int generation) {
        this.generation = generation;
    }

    /**
     *
     * @return
     */
    public boolean isAlive() {
        return alive;
    }

    /**
     *
     * @param alive
     */
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    /**
     *
     * @return
     */
    public double getMutation_rate() {
        return mutation_rate;
    }

    /**
     *
     * @param mutation_rate
     */
    public void setMutation_rate(float mutation_rate) {
        this.mutation_rate = mutation_rate;
    }

    /**
     *
     * @return
     */
    public Colour getColour() {
        return colour;
    }

    /**
     *
     * @param colour
     */
    public void setColour(Colour colour) {
        this.colour = colour;
    }

    /**
     *
     * @return
     */
    public double[] getTempAlleles() {
        return temp_alleles;
    }

    /**
     *
     * @param tempAlleles
     */
    public void setTempAlleles(double[] tempAlleles) {
            this.temp_alleles = tempAlleles;
    }

    /**
     *
     * @return
     */
    public double getLocal_temp() {
        return local_temp;
    }

    /**
     *
     * @param local_temp
     */
    public void setLocal_temp(double local_temp) {
        this.local_temp = local_temp;
    }

    /**
     *
     * @return
     */
    public double getResources() {
        return resources;
    }

    /**
     *
     * @param resources
     */
    public void setResources(float resources) {
        this.resources = resources;
    }
    //</editor-fold>

    //<editor-fold desc="Constructors">
    /**
     * Constructor to create progeny from parent
     * @param parent
     */
    public Daisy(Daisy parent) {
        this.dispersal = parent.dispersal;
        this.progeny = parent.progeny;
        this.age = 0;
        this.ploidy = parent.ploidy;
        this.generation = parent.generation + 1;
        this.alive = parent.alive;
        this.mutation_rate = parent.mutation_rate;
        this.colour = parent.colour;
        this.temp_alleles = parent.temp_alleles;
        this.local_temp = parent.local_temp;
    }

    /**
     * generic constructor
     * @param dispersal
     * @param progeny
     * @param mut_rate
     * @param colour
     * @param temp_alleles
     * @param local_temp
     */
    public Daisy(int dispersal, int progeny, double mut_rate, Colour colour, double[] temp_alleles, double local_temp) {
        this.dispersal = dispersal;
        this.progeny = progeny;
        this.age = 0;
        this.ploidy = 2;
        this.generation = 0;
        this.alive = true;
        this.mutation_rate = mut_rate;
        this.colour = colour;
        this.temp_alleles = temp_alleles;
        this.local_temp = local_temp;
        this.resources = new Random().nextInt(5);
    }
    //</editor-fold>


    /**
     * Method to grow the daisy and recalculate its resources
     * @param daisyMap daisyMap of daisies
     */
   public double grow(DaisyMap daisyMap) {
        // increment age
        this.age++;
        if (this.age > Constants.AGE_OF_DEATH) {
            this.alive = false;
            // remove daisy from tile and make sure daisyMap updates
            daisyMap.clearTile(this);
            return -1.0;
        }
        // calculate albedo
        double albedo_temp = Math.pow(((Constants.SOLAR_INTENSITY *
                radiation_factor(daisyMap) * daisyMap.getRadiationIntensity() *
                (1 - this.colour.getExpressedColour())/(4.0*Constants.SIGMA_CONSTANT))),(0.25));
        // realign albedo
        albedo_temp = albedo_temp + 25 - 234;
        // temperature is 70% new temp and 30% old temp
        this.local_temp = 0.7 * albedo_temp + 0.3 * local_temp;
        // change in resources is calculated
        double delta_resources = (5-Math.pow(local_temp - getOptimum(), 2));
        resources = resources + delta_resources;
        // make sure resources does not go below 0
        if (resources < 0) {
            resources = 0;
        }
        return resources;
   }

    /**
     *  Method to clonally reproduce daisies and place them on the daisymap
     * @param m the map object that the daisies are contained in
     * @param l the location of the parent daisy
     * @return an array of Daisy objects containing the progeny/children
     */
   public Daisy[] reproduce(DaisyMap m, Location l) {
       Daisy[] children = new Daisy[this.progeny];
       for(int i=0; i < this.progeny; i++) {
           // create daisy and assign its position
           Daisy child = new Daisy(this);
           Location newPos = m.getProgenyMapTile(l, this.dispersal);
           if (newPos != null) {
               // add daisy to map
               m.setDaisy(child, newPos);
           } else {
               // else break - no more space for children
               break;
           }
           child.resources = this.resources / (this.progeny + 1);
            // for each allele in colour, mutate
           child.mutateColour();
           // for each allele in temperature, mutate
           child.mutateTemp();
           // increase ploidy randomly
           Random r = new Random();
           if((1 + r.nextInt(1000)) < (Constants.PLOIDY_MUTATION_RATE * 1000)) {
               child.increasePloidy();
           }
           children[i]= child;
       }
       this.resources = this.resources / (this.progeny + 1);
        return children;
   }

    /**
     * Method that, upon random chance during reproduction, doubles the number of alleles that the daisy
     * posseses.
     */
   public void increasePloidy() {
       this.ploidy = this.ploidy * 2;
       double[] temp = temp_alleles;
       // double size of array
       temp_alleles = new double[temp.length * 2];
       // copy array contents into new one twice. [a,b] turns to [a,b,a,b]
       System.arraycopy(temp, 0, temp_alleles, 0, temp.length);
       System.arraycopy(temp, 0, temp_alleles, temp.length, temp.length);
   }

   /**
    * Method to mutate the colour alleles of a daisy, used during the mutations in reproduction
    */
   public void mutateColour() {
       this.colour.setAlleles(colour.mutate());
   }

    /**
     * Method to mutate the temperature alleles of a daisy, used during mutations in reproduction
     */
   public void mutateTemp() {
       mutate(temp_alleles, 0.1, 500);
   }

    static double[] mutate(double[] temp_alleles, double effect, int chance) {
        Random r = new Random();
        for (int i = 0; i < temp_alleles.length; i++) {
            // if 1 in 1000 random chance is satisfied, mutate
            if((1 + r.nextInt(chance)) < (Constants.MUTATION_RATE *1000)) {
                // randomly plus or minus 0.05 as a mutation
                temp_alleles[i] += effect * Math.pow(-1, r.nextInt(1));
            }
        }
        return temp_alleles;
    }

    /**
     * Returns the optimum temperature allele that would be expressed by the daisy due to DAE
     * Daisy expresses the closest temperature allele to the temperature locally.
     * @return
     */
   public double getOptimum() {
       double closest = temp_alleles[0];
       double distance = Math.abs(temp_alleles[0]- local_temp);

       for (double a: temp_alleles) {
           // if distance between a and local temp is smaller than closest and local temp
           if ((Math.abs(a - local_temp)) < distance) {
               closest = a;
               distance = Math.abs(a-local_temp);
           }
       }
       // by this point, closest contains the closest temperature allele to the given local temperature.
       return closest;
   }

    /**
     * Calculates the radiation factor of a given daisy - dependent on the vertical position on the map
     * @param m map in which the daisy is situated.
     * @return the calculated radiation factor for this daisy.
     */
   public double radiation_factor(DaisyMap m) {
       Location l = null;
       for(Map.Entry<Location, Daisy> entry : m.getAllTiles().entrySet()) {
           if( entry.getValue() == this) {
               l = entry.getKey();
           }
       }
       // this returned figure varies between 0.8 and 1.2 depending on their Y co-ord
     return (0.9 + (0.2 * ((double) l.getY_pos() / (double)m.getY_size())));
   }

    /**
     * Compares the resource collection of this daisy to the minimum needed to reproduce.
     * @return boolean value as to whether the daisy can reproduce or not.
     */
   public boolean canReproduce() {
       return resources  > Constants.RESOURCES_TO_REPRODUCE;
   }

    /**
     * toString method for a daisy.
     * @return
     */
   @Override
   public String toString() {
       StringBuilder str = new StringBuilder();
       str.append(local_temp).append(", ");
       str.append(Arrays.toString(this.temp_alleles)).append(", ").append(this.colour.toString()).append(", ");
       str.append(this.age).append(", ").append(this.ploidy);
       return str.toString();
   }




}
