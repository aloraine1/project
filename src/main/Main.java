package main;


import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Welcome to Daisyworld Simulation.\n This is a Java implementation of a Daisyworld model");
        System.out.println("WARNING: Running this program again will delete the data from the output folder.\n" +
                " Please save any needed data before proceeding.");
        System.out.println("Enter Y to proceed with a default simulation, or anything else to select variables: ");
        Scanner in = new Scanner(System.in);
        String def = in.nextLine();
        Simulation testSim;
        if (def.equals("Y")) {
            testSim = new Simulation();
        } else {
            System.out.println("Enter desired simulation length: ");
            int simLength = in.nextInt();
            System.out.println("Enter desired initial population of black daisies: ");
            int black = in.nextInt();
            System.out.println("Enter desired initial population of white daisies: ");
            int white = in.nextInt();
            System.out.println("Enter desired initial population of grey daisies: ");
            int grey = in.nextInt();
            System.out.println("Enter desired maximum X co-ordinate size: ");
            int xSize = in.nextInt();
            System.out.println("Enter desired maximum Y co-ordinate size: ");
            int ySize = in.nextInt();
            System.out.println("Enter initial temperature of map and daisies: ");
            double initialTemp = in.nextDouble();
            testSim = new Simulation(simLength, black, white, grey, xSize, ySize, initialTemp);
            System.out.println("Selections complete, starting simulation...");
        }
        testSim.start();


    }
}
