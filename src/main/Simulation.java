package main;

import java.io.*;
import java.util.Arrays;
import java.util.Map;

import static main.Constants.psdsd;

/**
 * Class to construct and run the whole simulation
 */
public class Simulation {

    private DaisyMap daisyMap;
    private int xSize;
    private int ySize;
    private int simLength;
    private int currentStep;
    private int initialPopulation;
    private int p1;

    /**
     * default simulation - given a fairly standard set of preset attributes defined in Contants class
     */
    public Simulation() {
        // clear old output files
        emptyOutputFolder();
        this.xSize = 50;
        this.ySize = 50;
        this.simLength = 20000;
        this.currentStep = 0;
        this.initialPopulation = 10;
        this.p1 = 1;
        // create daisyMap
        System.out.println("Setting up Daisyworld Simulation environment...");
        daisyMap = new DaisyMap(xSize, ySize, 25);
        daisyMap.addSetupDaisies(0,0, this.initialPopulation);
        daisyMap.setRadiationIntensity(1);
    }

    /**
     * Non default constructor - Allows setup to be customised.
     * @param simLength
     * @param grey
     * @param black
     * @param white
     * @param xSize
     * @param ySize
     * @param initialTemp
     */
    public Simulation(int simLength, int black, int white, int grey, int xSize, int ySize, double initialTemp) {
        emptyOutputFolder();
        this.xSize = xSize;
        this.ySize = ySize;
        this.simLength = simLength;
        this.initialPopulation = grey + black + white;
        this.p1 = 1;
        // create daisymap
        daisyMap = new DaisyMap(this.xSize, this.ySize, initialTemp);
        daisyMap.addSetupDaisies(black, white, grey);
        daisyMap.setRadiationIntensity(1);
    }

    /**
     * Method to start the running of the whole simulation and see it through to its completion
     */
    public void start() {
        // initial output
        // calculate radiation intensity
        System.out.println("Starting simulation...");
        for (int i = 0; i < simLength; i++) {
            // calculate radiation intensity
            double qwe = 400 - ((double)this.p1 / (double)36);
//          these lines below can be added to introduce the different radiation functions.
//            daisyMap.setRadiationIntensity(1 + 0.5 * (psdsd * Math.sin((3.14 * p1) / qwe)));
//            daisyMap.setRadiationIntensity( daisyMap.getRadiationIntensity() + (1.0/10000));
            // gradual increase of intensity after initial 500 steps
            if (i > 500) {
                p1++;
            }
            // update temperature map to smooth and assign temperatures to daisyless tiles
            daisyMap.updateTempMap();
            // run the main part of the simulation for each time step
            run();
            // output the x,y, colour, local_temp, progeny, t_opt, each temp_allele of each daisy
            if (currentStep % 50 == 0) {
                outputMap();
                // output the x,y, and temperature of each tile
                outputTMap();
            }
            // timestep output
            System.out.println("Running timestep " + i + ". Radiation Intensity: " + daisyMap.getRadiationIntensity());

        }
    }

    /**
     * Method to simulate the passing of a time-step
     */
    public void run() {
        // output data pre step
        if (this.currentStep % 1000 == 0) {
            output();
        }
        // Actual Simulation code starts here
        // grow all daisies and collect resources
        daisyMap.growDaisies();
        // perform reproduction
        daisyMap.reproduceDaisies();

        // cull daisies if over capacity
        if (daisyMap.getCarrying_capacity() > daisyMap.getPopulation()) {
            daisyMap.cullDaisies(daisyMap.getPopulation() - daisyMap.getCarrying_capacity());
        }
        currentStep++;
    }

    /**
     * method to output statistics about the model
     */
    public void output() {
        double globalAlbedo = daisyMap.getGlobalAlbedo();
        daisyMap.setGlobalAlbedo(daisyMap.calcGlobalAlbedo());
        // calculate global albedo
        daisyMap.setGlobalTemperature(daisyMap.calcAverageTemperature());
        // calculate temperature stdev
        daisyMap.setGlobalTemperatureSd(daisyMap.calcTemperatureSD());

        double white = 0.0, black =0.0 , grey = 0.0, avgOptTemp = 0.0, avgColour = 0.0;
        for (Map.Entry<Location, Daisy> entry : daisyMap.getAllTiles().entrySet()) {
            if (entry.getValue() != null) {
                if (entry.getValue().getColour().getExpressedColour() > 0.55) {
                    white++;
                } else if (entry.getValue().getColour().getExpressedColour() < 0.45) {
                    black++;
                } else {
                    grey++;
                }
                avgOptTemp += entry.getValue().getOptimum();
                avgColour += entry.getValue().getColour().getExpressedColour();
            }
        }
        avgOptTemp = avgOptTemp /  daisyMap.getAllTiles().size();
        avgColour = avgColour / daisyMap.getAllTiles().size();
        // add these to a file.
        StringBuilder str = new StringBuilder();
        str.append(currentStep).append(daisyMap.getGlobal_temperature()).append(",").append(daisyMap.getGlobalAlbedo());
        str.append(",").append(daisyMap.getGlobal_temperature_sd()).append(",").append(white).append(",").append(black).append(",");
        str.append(grey).append(",").append(avgOptTemp).append(",").append(avgColour).append("\n");
        // write to file for stats about
        writeToFile("output/outputFile.txt", str.toString());
    }

    /**
     * Outputs general map information at a given timestep to a file
     */
    public void outputMap() {
        File file = new File(("output/map" + this.currentStep));
        System.out.println("Outputting to file output/map" + this.currentStep);
        writeToFile("output/map" + this.currentStep, daisyMap.getPopulation() + "\n");
        for (Map.Entry<Location, Daisy> entry : daisyMap.getAllTiles().entrySet()) {
            if (entry.getValue() != null) {
                writeToFile("output/map" + this.currentStep, (entry.getKey().getX_pos() + ", " + entry.getKey().getY_pos() + ", " + entry.getValue().getColour().getExpressedColour() + ", " + entry.getKey().getLocal_temp() + ", " + entry.getValue().getOptimum()  + ", " + Arrays.toString(entry.getValue().getTempAlleles()) + ", " + entry.getValue().getResources()));
            } else {
                writeToFile("output/map" + this.currentStep, (entry.getKey().getX_pos() + ", " + entry.getKey().getY_pos() + ", " + entry.getKey().getLocal_temp()));
            }


        }
    }

    /**
     * Outputs the temperature at each co-ordinate at a given timestep
     */
    public void outputTMap() {
        File file = new File(("output/temps" + this.currentStep));
        System.out.println("Outputting to file output/temps" + this.currentStep);

        // for each timestep, a full printout is made for all locations.
        for (Map.Entry<Location, Daisy> entry : daisyMap.getAllTiles().entrySet()) {
            writeToFile("output/temps" + this.currentStep, (entry.getKey().getX_pos() + ", " + entry.getKey().getY_pos() + ", " + entry.getKey().getLocal_temp()));
        }
    }

    /**
     * Utility function to write the data to a file needed for the various output methods (Note: This appends files,
     *  does not wipe them).
     * @param filePath
     * @param content
     */
    public void writeToFile(String filePath, String content) {
        try (
            FileWriter fw = new FileWriter(filePath, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw)) {
            out.println(content);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    /**
     * Empties the folder where output is stored before running.
     */
    public void emptyOutputFolder() {
        System.out.println("Clearing output ready to start...");
        File folder = new File("output");
        for(File f: folder.listFiles()) {
            f.delete();
        }
    }

}
