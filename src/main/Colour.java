package main;

import java.util.Arrays;
import java.util.Random;

/**
 * Class to represent the colour alleles of a daisy
 *
 */
public class Colour {
    private double[] alleles;
    private double expressedColour;

    /**
     * Constructor that creates a colour obeject with a given ploidy
     * @param ploidy the sets of chromosomes
     */
    public Colour(int ploidy){
        // create new array for colour alleles of the size of the ploidy
        alleles = new double[ploidy];
        for(int i=0; i < alleles.length; i++) {
            alleles[i] = 0.5;
        }
        calculateExpressedColour();
    }

    /**
     * Constructor that takes ploidy and an array of alleles. It allows a copy of a colour object to be made
     * @param ploidy number of sets of chromosomes
     * @param alleles the array containing the different allele values
     */
    public Colour(int ploidy, double[] alleles) {
        this.alleles = new double[ploidy];
        this.alleles = alleles;
        calculateExpressedColour();
    }

    /**
     * Calculates the average colour expressed by the alleles. This is not done on DAE but as an average
     */
    public void calculateExpressedColour() {
        double total = 0;
        for (double a : alleles) {
            total += a;
        }
        this.expressedColour = total / alleles.length;
    }

    /**
     * Performs the random chance mutations on the array of colour values.
     */
    public double[] mutate() {
        return Daisy.mutate(alleles, 0.2, 1000);
    }

    /**
     */
    public double[] getAlleles() {
        return alleles;
    }

    /**
     * @param alleles
     */
    public void setAlleles(double[] alleles) {
        this.alleles = alleles;
    }

    /**
     *
     * @return
     */
    public double getExpressedColour() {
        return expressedColour;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return Arrays.toString(alleles);
    }

}