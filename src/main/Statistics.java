package main;


import java.util.Arrays;

/**
 * Simple statistics calculation class to be used to calculate basic statistics on temperature data
 */
public class Statistics {
    double[] data;
    int size;

    /**
     * Constructor
     * @param data
     */
    public Statistics(double[] data) {
        this.data = data;
        size = data.length;
    }

    /**
     * Calculate the mean value for data
     * @return the mean that has been calculated
     */
    double getMean() {
        double sum = 0.0;
        for(double a : data)
            sum += a;
        return sum/size;
    }

    /**
     * Calculate the variance
     * @return the variance
     */
    double getVariance() {
        double mean = getMean();
        double temp = 0;
        for(double a :data)
            temp += (a-mean)*(a-mean);
        return temp/(data.length-1);
    }

    /**
     * Calculates the standard deviation for data using the getVariance method.
     * @return the standard deviation
     */
    double getStdDev() {
        return Math.sqrt(getVariance());
    }

}


