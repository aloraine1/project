package main;

/**
 * A position Class to denote a position in the map of daisyworld
 * Contains X and Y co-ordinates
 */
public class Location {

    private int x_pos;
    private int y_pos;
    private double local_temp;


    /**
     *
     * @return
     */
    public int getY_pos() {
        return y_pos;
    }

    /**
     *
     * @param y_pos
     */
    public void setY_pos(int y_pos) {
        this.y_pos = y_pos;
    }

    /**
     *
     * @return
     */
    public int getX_pos() {
        return x_pos;
    }

    /**
     *
     * @param x_pos
     */
    public void setX_pos(int x_pos) {
        this.x_pos = x_pos;
    }

    /**
     *
     * @return
     */
    public double getLocal_temp() {
        return local_temp;
    }

    /**
     *
     * @param local_temp
     */
    public void setLocal_temp(double local_temp) {
        this.local_temp = local_temp;
    }

    /**
     * Constructor to create a new location instance
     * @param x the x size
     * @param y the y size
     * @param local_temp the temperature of all tiles upon construction
     */
    public Location(int x, int y, double local_temp) {
        this.x_pos = x;
        this.y_pos = y;
        this.local_temp = local_temp;
    }

    /**
     * Simple utility to see if one location is the same co-ordinates as another
     * @param pos the other position to compare this one against
     * @return whether the two co-ordinates match
     */
    public boolean equals(Location pos) {
        return (pos.getX_pos() == this.x_pos && pos.getY_pos() == this.y_pos);
    }

    /**
     * toString for location class
     * @return
     */
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("X: " + x_pos).append(", ").append("Y: " + y_pos).append(", ");
        str.append("TEMP: " + local_temp).append(", ");
        return str.toString();
    }
}
