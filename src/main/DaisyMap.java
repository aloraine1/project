package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Class to represent the map of Daisies that facilitates the simulation
 */
public class DaisyMap {

    // This is the main component of this class - it stores the locations with Daisies mapped to them
    private HashMap<Location, Daisy> allTiles;
    // size variables
    private int x_size;
    private int y_size;
    // statistic variables
    private double global_albedo;
    private double global_temperature;
    private double global_temperature_sd;
    private int carrying_capacity;
    // population - current number of daisies that are alive
    private int population;
    // radiation intensity of the map - set in simulation class
    private double radiationIntensity;


    //<editor-fold desc="Accessors and Mutators">

    /**
     *
     * @return
     */
    public HashMap<Location, Daisy> getAllTiles() {
        return allTiles;
    }

    /**
     *
     * @return
     */
    public int getX_size() {
        return x_size;
    }

    /**
     *
     * @param x_size
     */
    public void setX_size(int x_size) {
        this.x_size = x_size;
    }

    /**
     *
     * @return
     */
    public int getY_size() {
        return y_size;
    }

    /**
     *
     * @param y_size
     */
    public void setY_size(int y_size) {
        this.y_size = y_size;
    }

    /**
     *
     * @return
     */
    public double getGlobal_temperature_sd() {
        return global_temperature_sd;
    }

    /**
     *
     * @param global_temperature_sd
     */
    public void setGlobalTemperatureSd(double global_temperature_sd) {
        this.global_temperature_sd = global_temperature_sd;
    }

    /**
     *
     * @return
     */
    public double getGlobalAlbedo() {
        return global_albedo;
    }

    /**
     *
     * @param global_albedo
     */
    public void setGlobalAlbedo(double global_albedo) {
        this.global_albedo = global_albedo;
    }

    /**
     *
     * @return
     */
    public double getGlobal_temperature() {
        return global_temperature;
    }

    /**
     *
     * @param global_temperature
     */
    public void setGlobalTemperature(double global_temperature) {
        this.global_temperature = global_temperature;
    }

    /**
     *
     * @return
     */
    public int getCarrying_capacity() {
        return carrying_capacity;
    }

    /**
     *
     * @param carrying_capacity
     */
    public void setCarrying_capacity(int carrying_capacity) {
        this.carrying_capacity = carrying_capacity;
    }

    /**
     *
     * @return
     */
    public int getPopulation() {
        return population;
    }

    /**
     *
     * @param population
     */
    public void setPopulation(int population) {
        this.population = population;
    }

    /**
     *
     * @return
     */
    public double getRadiationIntensity() {
        return radiationIntensity;
    }

    /**
     *
     * @param radiationIntensity
     */
    public void setRadiationIntensity(double radiationIntensity) {
        this.radiationIntensity = radiationIntensity;
    }
    //</editor-fold>

    /**
     * Constructor to create a new map, supplied with the size and initial temperature
     * @param x_size
     * @param y_size
     * @param initial_temp
     */
    public DaisyMap(int x_size, int y_size, double initial_temp) {
        this.x_size = x_size;
        this.y_size = y_size;
        // create the hashmap object
        allTiles = new HashMap<Location, Daisy>();
        // for each possible co-ordinate set, add a location object with the correct info.
        for (int x = 0; x < x_size; x++) {
            for (int y = 0; y < y_size; y++) {
                allTiles.put(new Location(x,y, initial_temp), null);
            }
        }
    }

    /**
     * Method to set the Value of the Hashmap key of the location l to be the daisy d
     * @param d Daisy to add to the Map
     * @param l Location to assign it to
     * @return
     */
    public boolean setDaisy(Daisy d, Location l) {
        // replace the null value for this key with d
        this.allTiles.put(l,d);
        // add to the population value
        population++;
        return true;
    }

    /**
     * Removes the daisy from its tile on the map upon its death
     * @param d daisy to remove
     */
    public void clearTile(Daisy d) {
        for(Map.Entry<Location, Daisy> entry : allTiles.entrySet()) {
            // if daisy matches -> set it to null
            if( entry.getValue() == d) {
                entry.setValue(null);
            }
        }
        population--;
        // Although hashmap values are not unique, this works as each daisy is created and assigned different memory,
        // and no daisy occurs in the map more than once.
    }

    /**
     * Method to assign a number of defaulted daisies to the map at the start of the program.
     * It places them in the central region of the map.
        @param black
        @param white
        @param grey
     */
    public void addSetupDaisies(int black, int white, int grey) {
        Random r = new Random();
        ArrayList<Location> availableLocations = new ArrayList<>();
        // get available locations of daisies
        for (Map.Entry<Location, Daisy> entry : allTiles.entrySet()) {
            if(entry.getValue() == null && entry.getKey().getY_pos() > (y_size / 2) - 2 && entry.getKey().getY_pos() < (y_size / 2)) {
                availableLocations.add(entry.getKey());
            }
        }
        for (int i = 0; i < black; i++) {
            // get one of the available locations
            int randomIndex = r.nextInt(availableLocations.size());
            // add a new daisy with default attributes to this location
            this.setDaisy(new Daisy(Constants.INITIAL_DISPERSAL,
                            Constants.INITIAL_PROGENY,
                            Constants.MUTATION_RATE,
                            new Colour(Constants.INITIAL_PLOIDY, new double[]{0.1,0.1}),
                            Constants.INITIAL_TEMPERATURE_DIFF,
                            availableLocations.get(randomIndex).getLocal_temp()), availableLocations.get(randomIndex));
            // make this location no longer available.
            availableLocations.remove(randomIndex);
        }
        for (int i = 0; i < white; i++) {
            // get one of the available locations
            int randomIndex = r.nextInt(availableLocations.size());
            // add a new daisy with default attributes to this location
            this.setDaisy(new Daisy(Constants.INITIAL_DISPERSAL,
                    Constants.INITIAL_PROGENY,
                    Constants.MUTATION_RATE,
                    new Colour(Constants.INITIAL_PLOIDY, new double[]{0.9,0.9}),
                    Constants.INITIAL_TEMPERATURE_DIFF,
                    availableLocations.get(randomIndex).getLocal_temp()), availableLocations.get(randomIndex));
            // make this location no longer available.
            availableLocations.remove(randomIndex);
        }
        for (int i = 0; i < grey; i++) {
            // get one of the available locations
            int randomIndex = r.nextInt(availableLocations.size());
            // add a new daisy with default attributes to this location
            this.setDaisy(new Daisy(Constants.INITIAL_DISPERSAL,
                    Constants.INITIAL_PROGENY,
                    Constants.MUTATION_RATE,
                    new Colour(Constants.INITIAL_PLOIDY, new double[]{Constants.INITIAL_COLOUR, Constants.INITIAL_COLOUR}),
                    Constants.INITIAL_TEMPERATURE_EVEN,
                    availableLocations.get(randomIndex).getLocal_temp()), availableLocations.get(randomIndex));
            // make this location no longer available.
            availableLocations.remove(randomIndex);
        }
    }

    /**
     * Finds and returns a random location that is not inhabited by a daisy
     * @return
     */
    public Location getRandomEmptyPosition() {
        Random r = new Random();
        ArrayList<Location> emptyLocations = new ArrayList<>();
        for(Map.Entry<Location, Daisy> entry : allTiles.entrySet()) {
            // if value is null, no daisy, add to list.
            if (entry.getValue() == null) {
                emptyLocations.add(entry.getKey());
            }
        }
        int randomIndex = r.nextInt(emptyLocations.size());
        return emptyLocations.get(randomIndex);
    }

    /**
     * Finds and returns a random map tile within the dispersal radius
     * @param pos location of the parent
     * @param dispersal the dispersal radius that the new location has to be in
     * @return the progeny location that was found.
     */
    public Location getProgenyMapTile(Location pos, int dispersal) {
        int minx, miny, maxx, maxy;
        // assign min and max values for x and y, ensuring that they are inside the map constraints
        if (pos.getX_pos() - dispersal < 0) {
            minx = pos.getX_pos() - dispersal;
        } else {
            minx = 0;
        }
        if (pos.getY_pos() - dispersal < 0) {
            miny = pos.getY_pos() - dispersal;
        } else {
            miny = 0;
        }
        if (pos.getX_pos() + dispersal > x_size) {
            maxx = pos.getX_pos() + dispersal;
        } else {
            maxx = x_size - 1;
        }
        if (pos.getY_pos() + dispersal > y_size) {
            maxy = pos.getY_pos() + dispersal;
        } else {
            maxy = y_size -1;
        }
        Random r = new Random();
        // add locations to progeny locations when they are empty and within the bounds set above
        ArrayList<Location> progenyLocations = new ArrayList<Location>();
        for(Map.Entry<Location, Daisy> entry : allTiles.entrySet()) {
            if (entry.getValue() == null) {
                Location loc = entry.getKey();
                // if location is within the bounds, add it to progenyLocations
                if ((minx < loc.getX_pos() && loc.getX_pos() < maxx) && (miny < loc.getY_pos() && loc.getY_pos() < maxy)) {
                    progenyLocations.add(loc);
                }
            }
        }
        if (progenyLocations.isEmpty()) {
            return null;
        } else {
            // get random from progenyLocations
            int randomIndex = r.nextInt(progenyLocations.size());
            Location randomProgenyLocation = progenyLocations.get(randomIndex);
            return randomProgenyLocation;
        }
    }

    /**
     * Two main tasks are completed here. The locations without daisies are updated, and then all tiles are smoothed
     * out to simulate the normal diffusion of heat across an area.
     */

    public void updateTempMap() {
        int minx, miny, maxx, maxy = 0;
        for (Map.Entry<Location, Daisy> entry : allTiles.entrySet()) {
            if (entry.getValue() != null) {
                // if daisy is present, its local temp should be moved to the location
                entry.getKey().setLocal_temp(entry.getValue().getLocal_temp());
            } else {
                // if there is no daisy present - calculate temperature using base albedo of 0.5
                double albedo_temp = Math.pow((Constants.SOLAR_INTENSITY * (0.9 + (0.2 * (((double) entry.getKey().getY_pos()) / (double) this.y_size))) * this.radiationIntensity * 0.5) / (4 * Constants.SIGMA_CONSTANT), 0.25);
//                System.out.println("ALBEDO TEMP = ((" + Constants.SOLAR_INTENSITY + " * " + (0.9 + (0.2 * (((double)entry.getKey().getY_pos())) / (double)this.y_size)) + " * " + this.radiationIntensity + " * " + 0.5 + ") / " + (4 * Constants.SIGMA_CONSTANT) + ")^1/4");
//                System.out.println(" == " + albedo_temp);
                albedo_temp = albedo_temp + 25 - 234;
//                System.out.println("After scale down: " + albedo_temp);
                entry.getKey().setLocal_temp(0.7 * albedo_temp + 0.3 * entry.getKey().getLocal_temp());

            }
//            System.out.println("Pre Smoothing: " + entry.getKey().getLocal_temp());
            // all locations must be considered in the smoothing process

            // get area around location to smooth over
            Location loc = entry.getKey();
            if (loc.getX_pos() < 1) {
                minx = 0;
            } else {
                minx = entry.getKey().getX_pos() - 1;
            }
            if (loc.getX_pos() > x_size) {
                maxx = x_size;
            } else {
                maxx = loc.getX_pos() + 1;
            }
            if (loc.getY_pos() < 1) {
                miny = 0;
            } else {
                miny = entry.getKey().getY_pos() - 1;
            }
            if (loc.getY_pos() > y_size) {
                maxy = y_size;
            } else {
                maxy = loc.getY_pos() + 1;
            }
            ArrayList<Location> smoothingLocations = new ArrayList<>();
            for (Map.Entry<Location, Daisy> allEntries : allTiles.entrySet()) {
                Location location = allEntries.getKey();
                // if location is within the bounds, add it to progenyLocations
                if ((minx <= location.getX_pos() && location.getX_pos() <= maxx) && (miny <= location.getY_pos() && location.getY_pos() <= maxy)) {
                    smoothingLocations.add(loc);
                }
            }
            // count up total temperature of the area
            double total = 0;

            for (Location l : smoothingLocations) {
                total += l.getLocal_temp();
            }
            // all tiles in the immediate area are assigned the average value of the surrounding tiles
            for (Location l : smoothingLocations) {
                l.setLocal_temp(total / smoothingLocations.size());
                if (allTiles.get(l) != null) {
                    // if there is a daisy present, change the daisy temp too
                    allTiles.get(l).setLocal_temp(total / smoothingLocations.size());
                }
            }
            // clear the close locations to ensure they are not reused.
            smoothingLocations.clear();
        }
    }

    /**
     * Calculates the mean temperature across the map for statistical analysis
     */
    public double calcAverageTemperature() {
        collectTemperatureDataToDoubleArray();
        Statistics s = new Statistics(collectTemperatureDataToDoubleArray());
        return s.getMean();
    }

    /**
     * Gets temperatures on the map into a double array so that it can be used for statistical analysis
     * @return
     */
    private double[] collectTemperatureDataToDoubleArray() {
        double[] allData = new double[allTiles.size()];
        int i = 0;
        for(Map.Entry<Location, Daisy> entry : allTiles.entrySet()) {
            if (entry.getValue() == null) {
                allData[i] = entry.getKey().getLocal_temp();
            } else {
                allData[i] = entry.getValue().getLocal_temp();
            }
            i++;
        }
        return allData;
    }

    /**
     * Calculates the standard deviation of the temperature values across the map
     * @return
     */
    public double calcTemperatureSD() {
        Statistics s = new Statistics(collectTemperatureDataToDoubleArray());
        return s.getStdDev();
    }

    /**
     * Calculates the albedo of the whole planet
     * @return the albedo across the planet
     */
    public double calcGlobalAlbedo() {
        double global_albedo = 0;
        for (Map.Entry<Location, Daisy> entry : allTiles.entrySet()) {
            if (entry.getValue() != null) {
                if (entry.getValue().isAlive()) {
                    global_albedo += entry.getValue().getColour().getExpressedColour();
                }
            }
        }
        global_albedo += 0.5 * (x_size * y_size - population);
        return global_albedo / (x_size * y_size);
    }

    /**
     * loops through all daisies and runs the grow function on all of them.
     */
    public void growDaisies() {
        // grow all daisies.
        for(Map.Entry<Location, Daisy> entry : allTiles.entrySet()) {
            if (entry.getValue() != null) {
                if(entry.getValue().isAlive()) {
                   entry.getValue().grow(this);
                }
            }
        }
    }

    /**
     * loops through all daisies and runs the reproduce function on them if they meet the conditions/
     */
    public void reproduceDaisies() {
        // if they are suitable, reproduce all daisies
        for(Map.Entry<Location, Daisy> entry : allTiles.entrySet()) {
            if (entry.getValue() != null && entry.getValue().canReproduce()) {
                entry.getValue().reproduce(this, entry.getKey());
            }
        }
    }

    /**
     * Removes a random selection of daisies
     * @param numberToCull the number of daisies to remove
     */
    public void cullDaisies(int numberToCull) {
        for (int i= 0; i <= numberToCull; i++) {
            ArrayList<Location> keysArray = new ArrayList<Location>(allTiles.keySet());
            Random r = new Random();
            allTiles.put(keysArray.get(r.nextInt(keysArray.size())), null);
        }
    }

    /**
     * toString method for the daisy map
     * @return
     */
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(x_size).append(", ").append(y_size).append(", ");
        str.append("[");
        for(Map.Entry<Location, Daisy> entry : allTiles.entrySet()) {
            str.append(entry.getKey().toString());
            if (entry.getValue() != null) {
                str.append(entry.getValue().toString()).append(", ");
            }

        }
        str.append("]");
        return str.toString();
    }

}
