package main;

public class Constants {
    // Age at which daisies die.
    public static final int AGE_OF_DEATH = 50;
    // Base solar intensity
    public static final float SOLAR_INTENSITY = 1366; // Wm^-2
    // Mathematical constant
    public static final double SIGMA_CONSTANT = 5.67 * Math.pow(10, -8);
    // Base radiation intensity
    public static double RADIATION_INTENSITY = 1;
    // Base mutation rate used for temperature and colour mutations
    public static double MUTATION_RATE = 0.05;
    // 0.01 for more common, 0.005 for less common
    public static double PLOIDY_MUTATION_RATE = 0.01;
    // relative measure of how much resource generation needs to happen prior to reproducing
    public static int RESOURCES_TO_REPRODUCE = 30;
    // initial setup data for daisy constructors
    public static double INITIAL_COLOUR = 0.5;
    public static double[] INITIAL_TEMPERATURE_EVEN = {25,25};
    public static double[] INITIAL_TEMPERATURE_DIFF = {23,27};
    public static int INITIAL_DISPERSAL = 3;
    public static int INITIAL_PROGENY = 4;
    public static int INITIAL_POPULATION = 10;
    public static int INITIAL_PLOIDY = 2;

    // used in the oscillating radiation intensity function.
    public static double psdsd = 0.3;
}