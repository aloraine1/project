package Test;

import main.Daisy;
import main.DaisyMap;
import main.Location;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class DaisyMapTest {

    @Test
    public void clearTileRemovesDaisy() {
        DaisyMap dm = new DaisyMap(10,10,24);
        dm.addSetupDaisies(0,0,5);
        for (Map.Entry<Location, Daisy> entry : dm.getAllTiles().entrySet()) {
            if (entry.getValue() != null) {
                dm.clearTile(entry.getValue());
                // ensure that the value of this daisy is null
                assertEquals(null, entry.getValue());
                break;
            }
        }
        // ensure that the value of the map's population has changed
        assertEquals(4, dm.getPopulation());
    }

    @Test
    public void addSetupDaisiesAddsTheRightAmountOfDaisies() {
        DaisyMap dm = new DaisyMap(10,10,24);
        dm.addSetupDaisies(0,0,5);
        assertEquals(5, dm.getPopulation());
    }

    @Test
    public void getRandomEmptyPositionGivesEmptyPosition() {
        DaisyMap dm = new DaisyMap(10,10, 24);
        // ensure not all locations are empty
        dm.addSetupDaisies(0,0,5);
        Location l = dm.getRandomEmptyPosition();
        Location l2 = dm.getRandomEmptyPosition();
        Location l3 = dm. getRandomEmptyPosition();
        for(Map.Entry<Location, Daisy> entry : dm.getAllTiles().entrySet()) {
            if( entry.getKey() == l || entry.getKey() == l2 || entry.getKey() == l3) {
                // for any of these locations, the daisy should be null
                assertEquals(null, entry.getValue());
            }
        }
    }

    @Test
    public void calcAverageTemperature() {
        DaisyMap dm = new DaisyMap(10,10,24);
        // all daisies are set up to use 24 as default temperature.
        dm.addSetupDaisies(0,0,5);
        assertEquals(24, dm.calcAverageTemperature(), 0.01);
    }

    @Test
    public void calcTemperatureSD() {
        DaisyMap dm = new DaisyMap(10, 10, 24);
        dm.addSetupDaisies(0,0,5);
        // stdev of all the same temp should be 0.
        assertEquals(0, dm.calcTemperatureSD(), 0.01);
    }
}