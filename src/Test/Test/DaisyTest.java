package Test;

import main.Colour;
import main.Constants;
import main.Daisy;
import org.junit.Test;

import static org.junit.Assert.*;

public class DaisyTest {

    /**
     * Test to see if the double ploidy function behaves as expectd, doubling the number of allelles
     */
    @Test
    public void increasePloidyDoublesPloidy() {
        Daisy d = new Daisy(Constants.INITIAL_DISPERSAL,
                Constants.INITIAL_PROGENY,
                Constants.MUTATION_RATE,
                new Colour(Constants.INITIAL_PLOIDY, new double[]{Constants.INITIAL_COLOUR, Constants.INITIAL_COLOUR}),
                Constants.INITIAL_TEMPERATURE_EVEN, 24);
        d.increasePloidy();
        assertEquals(4, d.getPloidy());
        d.increasePloidy();
        assertEquals(8, d.getPloidy());
    }

    /**
     * Test to check that the optimum temperatue picker is picking the closest temperature allele to the current temp
     */
    @Test
    public void getOptimumPicksClosestToOptimum() {
        Daisy d = new Daisy(Constants.INITIAL_DISPERSAL,
                Constants.INITIAL_PROGENY,
                Constants.MUTATION_RATE,
                new Colour(Constants.INITIAL_PLOIDY, new double[]{Constants.INITIAL_COLOUR, Constants.INITIAL_COLOUR}),
                new double[]{24, 26},  27);
        assertEquals(26 , d.getOptimum(), 0.001);
    }

    /**
     * Test to see if the canReproduce measures the resource value correctly and behaves as expected
     */
    @Test
    public void canReproduceReturnsCorrectly() {
        Daisy d = new Daisy(Constants.INITIAL_DISPERSAL,
                Constants.INITIAL_PROGENY,
                Constants.MUTATION_RATE,
                new Colour(Constants.INITIAL_PLOIDY, new double[]{Constants.INITIAL_COLOUR, Constants.INITIAL_COLOUR}),
                new double[]{24, 26},  27);
        assertEquals(false, d.canReproduce());
        d.setResources(Constants.RESOURCES_TO_REPRODUCE + 1);
        assertEquals(true, d.canReproduce());
    }


}