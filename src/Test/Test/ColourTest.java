package Test;

import main.Colour;
import org.junit.Test;

import static org.junit.Assert.*;

public class ColourTest {

    @Test
    public void calculateExpressedColour() {
        Colour c = new Colour(2, new double[]{0.5, 0.5});
        assertEquals(0.5, c.getExpressedColour(), 0.001);
        Colour c2 = new Colour(2, new double[]{0.5, 1});
        assertEquals(0.75, c2.getExpressedColour(), 0.001);
        Colour c3 = new Colour(4, new double[]{0.5,0.5,1,1});
        assertEquals(0.75, c3.getExpressedColour(), 0.001);
    }
}